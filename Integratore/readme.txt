per avviare il programma lanciare lo script "start.sh"
per cambiare le condizioni iniziali ed i parametri modificare il file "condizioni_iniziali.dat"
per cambiare l'equazione differenziale da integrare modificare la macro nel file sorgente "integratore.c"

i plot vengono salvati in formato ".png" nella directory di esecuzione (se è definita la variabile da precompilatore "PNG")
