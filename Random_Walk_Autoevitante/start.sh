#!/bin/bash
# Coded by Pietro Squilla & Francesco Servilio

# entro dentro la cartella con gli eseguibili
DIR=$(dirname "$0")
cd $DIR

killall gnuplot_x11 -q 
killall gnuplot_qt -q 

# compilo ed eseguo il programma
gcc rwa.c -o rwa.exe -lm -pedantic -Wall -ffast-math -O2

date

time ./rwa.exe -D DEBUG=0 &

wait $!
rm rwa.exe

# elimina le immagini vuote
find -size 0 -name "*.png" -delete

exit 0
