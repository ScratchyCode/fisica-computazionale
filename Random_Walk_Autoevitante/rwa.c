// Scritto da Pietro Squilla e Francesco Valerio Servilio
#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <time.h>
#include <limits.h>
#include <string.h>
#include <unistd.h>

#define PNG 1
#define pSize 1200


typedef struct punto{
    long int x;
    long int y;
} punto;

void checkPtr(void *ptr);
void transiente (int scarti, double (*metodo)(void));
char **createMatrix(long long int rows, long long int columns);
void freeMatrix(long long int rows, char **M);
void initMatrix(long long int rows, long long int columns, char **M, char value);
void init_pipe(FILE *pipe, char *xlabel, char *ylabel, char *titolo, char *nome_grafico, char *plotCmd);
void chiudiPipe(FILE *pipe);
void controllo_gnuplot(void);

int main(){
    unsigned int drawn, binsize=0;
    unsigned long int Nsteps, Nstorie, i, k, count=0, tmin, tmax;
    punto pos;
    
    controllo_gnuplot();
    
    FILE *input = fopen("condizioni_iniziali.dat","r");
    FILE *output = fopen("rwa.dat","w");
    checkPtr(input);
    checkPtr(output);
    
    fscanf(input,"%lu %lu %u \n", &Nstorie, &Nsteps, &binsize);
    fclose(input);
    
    srand48(time(NULL));
    transiente(12345, drand48);
    
    char **ret = createMatrix(Nsteps*2+3,Nsteps*2+3);  // matrice di tipo char per occupare meno memoria (un char occupa 8 bit)
    checkPtr(ret);                                     // reticolo quadrato di lato 2*Nsteps+3 inizializzato a 0
    unsigned long int *trap=calloc(Nsteps+1,sizeof(unsigned long int));    
    checkPtr(trap);
    tmin=Nsteps;
    tmax=0;
    
    for(k=1;k<=Nstorie;k++){
        
        initMatrix(Nsteps*2+3, Nsteps*2+3, ret, 0);
        
        // posizione iniziale al centro
        pos.x=(int)(Nsteps+1);
        pos.y=(int)(Nsteps+1);
        ret[pos.x][pos.y]=2;
        
        // spostamento della particella
        for(i=1; i<=Nsteps; i++){

            // 2: la particella ha già occupato il posto o occupa attualmente il posto
            // 0: la particella non ha mai occupato il posto
            
            drawn = (int)(4.*drand48());

            if(drawn == 0 && ret[pos.x][pos.y+1] == 0){
                // se è libero sopra -> sposto la particella verso l'alto
                pos.y++;
                ret[pos.x][pos.y] = 2;
                
            }else if(drawn == 1 && ret[pos.x][pos.y-1] == 0){
                // se è libero sotto -> sposto la particella verso il basso
                pos.y--;
                ret[pos.x][pos.y] = 2;
                
            }else if(drawn == 2 && ret[pos.x+1][pos.y] == 0){
                // se è libero a destra -> sposto la particella a destra
                pos.x++;
                ret[pos.x][pos.y] = 2;
                
            }else if(drawn == 3 && ret[pos.x-1][pos.y] == 0){
                // se è libero a sinistra -> sposto la particella a sinistra
                pos.x--;
                ret[pos.x][pos.y] = 2;
                
            }else if(drawn > 3)printf("\nERROR: which is the neighbour?\n");
            else if(( (ret[pos.x][pos.y+1] == 2) + (ret[pos.x][pos.y-1] == 2) + (ret[pos.x+1][pos.y] == 2) + (ret[pos.x-1][pos.y] == 2) ) == 4){
                // la particella si è intrappolata
                count++;
                trap[i]++;
                fprintf(output, "%lu %lu \n", count, i);
                if(i<tmin) tmin=i;
                else if(i>tmax) tmax=i;
                break;
            }else i--; // se la direzione scelta è occupata rilancia il dado
        }
    }
    
    freeMatrix(Nsteps*2+3, ret);

    printf("\nLa particella si e intrappolata %lu volte in %lu storie\n", count, Nstorie); 
    
    
    /* istogramma */
    FILE *ISTO = popen("/usr/bin/gnuplot -persist 2> /dev/null","w");
    init_pipe(ISTO,"\"t\"","\"P(t)\"","\"Istogramma dello step a cui la particella si e intrappolata\"","\"isto.png\"","\n");

    if(!binsize){
        printf("\nBinsize is null!\n");
        exit(0);
    }
    
    fprintf(ISTO,"set boxwidth %u absolute \n  set style fill solid border -1 \n", binsize);
    fprintf(ISTO,"set autoscale xy \n");
    fprintf(ISTO,"bin(x)=%u*( floor( (x-%lu)/%u ) + 0.5 ) + %lu \n", binsize, tmin, binsize, tmin);
    fprintf(ISTO,"plot '-' u (bin($1)):(1) smooth freq w boxes lc rgb 'red' title 'occorrenze'");
    
    for(k=0;k<Nsteps;k++){
        for(i=1;i<=trap[k];i++) {
            fprintf(ISTO,"%lu \n", k); // plotto istogramma
        }
    }
    chiudiPipe(ISTO);
    /* istogramma */
    
    
    free(trap);
    
    return 0;
}

void checkPtr(void *ptr){
    if (ptr==NULL){
        perror("\nERROR");
        fprintf(stderr,"\n");
        exit(0);
    }
}

void transiente (int scarti, double (*metodo)(void)){
    int i;
    
    for(i=0; i<scarti; i++)
        metodo();
    
    return;
}


char **createMatrix(long long int rows, long long int columns){
    long long int i;
    
    char **M = (char **) calloc(rows,sizeof(char *));
    checkPtr(M);
    
    for(i=0; i<rows; i++){
        M[i] = (char *) calloc(columns,sizeof(char));
        checkPtr(M[i]);
    }
    
    return M;
}

void initMatrix(long long int rows, long long int columns, char **M, char value){
    long long int i, j;
    
    for(i=0; i<rows; i++) for(j=0; j<columns; j++) M[i][j] = value;
    
    return;
}

void freeMatrix(long long int rows, char **M){
    
    while(--rows > -1){
        free(M[rows]);
    }
    
    free(M);
    
    return ;
}

void init_pipe(FILE *pipe, char *xlabel, char *ylabel, char *titolo, char *nome_grafico, char *plotCmd){
    
    fprintf(pipe,"set autoscale \n");
    
    #ifdef PNG
    fprintf(pipe,"set terminal png size %d,%d\n",(int)pSize,(int)(pSize*0.9133333)); // imposta la risoluzione dell'immagine in base alla dimensione della griglia di incrementi
    #else
    fprintf(pipe,"set terminal x11 \n");
    #endif  
    
    fprintf(pipe,"set xlabel %s \n",xlabel);
    fprintf(pipe,"set ylabel %s \n",ylabel);
    fprintf(pipe,"set title %s \n",titolo);
    
    #ifdef PNG
    fprintf(pipe,"set output %s \n",nome_grafico);
    #endif  
    
    if(!strcmp(plotCmd,"")) fprintf(pipe,"plot '-' u 1:2 w l \n");
    else fprintf(pipe,"%s \n", plotCmd);
    
    return;
}

void chiudiPipe(FILE *pipe){
    
    fflush(pipe);
    fprintf(pipe,"quit\n");
    pclose(pipe);
    
    return;
}


void controllo_gnuplot(void){
    FILE *output = fopen("/usr/bin/gnuplot","r");
    
    if(output == NULL){
        printf("\nInstalla gnuplot con: sudo apt install gnuplot\n\n");
        exit(1);
    }
    
    return;
}

