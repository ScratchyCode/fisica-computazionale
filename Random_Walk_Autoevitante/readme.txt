simula un random walk autoevitante in 2 dimensioni
salva su file "rwa.dat" lo step a cui la particella si è intrappolata

dagli istogrammi si evince che, per un reticolo bidimensionale, la massima probabilità
di intrappolamento si ha a circa 45 step, per cui per fare in modo che la particella si
intrappoli con buone probabilità, basta impostare Nstep=45: andare oltre tale valore non è necessario
