// Scritto da Pietro Squilla e Francesco Valerio Servilio
#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <time.h>
#include <limits.h>
#include <string.h>
#include <unistd.h>

// #define DEBUG 1
#define PRINTDATI
#define PNG 1 
#define pSize 2000.0 
#define D 2    // D deve essere una variabile da precompilatore altrimenti il codice risulta poco ottimizzato

typedef long long int INT; // %lld
typedef unsigned long long int UINT; // %llu

void transiente(int scarti, double (*metodo)(void));
double ecuyer1(void);
void controllo_gnuplot(void);
void init_pipe(FILE *pipe, char *xlabel, char *ylabel, char *titolo, char *nome_grafico, char *plotCmd);
void chiudiPipe(FILE *pipe);
INT **createMatrix(long long int rows, long long int columns);
void freeMatrix(long long int rows, INT **M);
void checkPtr(void *ptr);
double rand1(void);
double random1(void);
double minstd(void);
UINT initSeed(int a);

double ULL = 1./(ULLONG_MAX+1.);
UINT I;

int main(void) {
    int Nstorie, Nsteps, algo;
    int seed = time(0);
    int i, j, k, dir, binsize=0;
    long double tmp;
    
    // scanf da file
    FILE *input = fopen("condizioni_iniziali.dat","r");
    checkPtr(input);
    fscanf(input,"%d %d %d %d\n", &Nstorie, &Nsteps, &algo, &binsize);
    fclose(input);
    
    
    controllo_gnuplot();
    
    INT *x = (INT*)calloc(D,sizeof(INT));
    int xSize = D*sizeof(INT); // calcolo la lunghezza del vettore posizione
    UINT *distanze = (UINT*)calloc(Nsteps,sizeof(UINT));
    double (*metodo)(void);
    FILE *DATI = fopen("rw.dat","w");
    FILE *DISTANZE = popen("/usr/bin/gnuplot -persist 2> /dev/null","w");
    
    init_pipe(DISTANZE,"\"t\"","\"<x^2>\"","\"media (sulle storie) delle distanze euclidee al quadrato in funzione di t (step): scala doppio-log\"","\"distanze.png\"","set logscale xy \n plot x lw 1 title 'y=x', '-' u 1:2 ps 0.6 pointtype 7 lc rgb 'blue' title 'distanze al quadrato' ");
    
    // istogrammi
    #if D==1
    INT xmax=-Nsteps, xmin=Nsteps;
    INT *xfinale=(INT*)calloc(Nstorie,sizeof(INT));
    checkPtr(xfinale);
    
    FILE *ISTO = popen("/usr/bin/gnuplot -persist 2> /dev/null","w");
    init_pipe(ISTO,"\"x\"","\"P(x_{finale})\"","\"Istogramma posizioni finali\"","\"isto.png\"","\n");
    #endif
    
    #if D==2
    INT *xmax=(INT*)calloc(D,sizeof(INT)), *xmin=calloc(D,sizeof(INT));
    for (i=0;i<D;i++){
        xmax[i]=-Nsteps;
        xmin[i]=Nsteps;
    }
    INT **xfinale = createMatrix(Nstorie,D);
    checkPtr(xfinale);
    
    FILE *ISTO = popen("/usr/bin/gnuplot -persist 2> /dev/null","w");
    init_pipe(ISTO,"\"x_{finale}[0]\"","\"x_{finale}[1]\"","\"Istogramma 3D posizioni finali\"","\"isto.png\"","\n");
    #endif
    
    // scelta generatore
    switch(algo){
        default:
            metodo = drand48;
            srand48(seed);
            break;
        case 1:
            metodo = drand48;
            srand48(seed);
            break;
        case 2:
            metodo = ecuyer1;
            I = seed;
            break;
    }
    
    
    transiente(12345,metodo);
    
    
    for(j=0; j<Nstorie; j++){
        
        memset(x,0,xSize); // azzero il vettore posizione
        
        for(i=0; i<Nsteps; i++){
            dir = (int)(metodo()*2.*D); // estraggo la direzione di movimento (# di direzioni possibili = 2*D)
            x[(int)(dir/2.)] += (int)((dir%2-0.5)*2.); // modifico il vettore posizione in base alla direzione estratta
            
            #ifdef PRINTDATI
            fprintf(DATI,"S %d T %d",j+1,i+1);
            for(k=0;k<D;k++){
                fprintf(DATI," x%d %lld",k+1,x[k]);
                distanze[i] += x[k]*x[k]; // salvo distanza euclidea al quadrato dall'origine
            }
            fprintf(DATI," \n");
            #endif    
            
        }
        
        #if D==1
        if(x[0] > xmax) xmax = x[0];
        else if(x[0] < xmin) xmin = x[0]; 
        xfinale[j] = x[0]; //x finale per ogni storia
        #endif
        
        #if D==2
        for (i=0;i<D;i++){
            if(x[i] > xmax[i]) xmax[i] = x[i];
            else if(x[i] < xmin[i]) xmin[i] = x[i]; 
            xfinale[j][i] = x[i]; //x finale per ogni storia
        }  
        #endif        
        
    }
    
    fclose(DATI); // chiudo il file di dati
    
    #ifdef PRINTDATI
    for(i=0; i<Nsteps; i++) fprintf(DISTANZE,"%d %.15lf\n",i,(double)distanze[i]/Nstorie);     //plotta le distanze euclidee
    #endif
    
    
    /* istogramma 2D */
    #if D==1
    if(!binsize) { // dipendenza trovata empiricamente
        if(Nstorie>=Nsteps) binsize = (int)(3.*pow(Nsteps,2./3.)*pow(Nstorie,2./3.)/20000.);
        if(Nstorie<Nsteps) binsize = (int)(3.*Nstorie/120.);
    }
    int Nbin = (int)((xmax-xmin+1.)/binsize)+1;
    double *h = (double*)calloc(Nbin+2,sizeof(double));
    
    #ifdef DEBUG
    printf ("\nxfinale1=%lld, xmin=%lld, xmax=%lld",xfinale[1],xmin,xmax);
    printf("\nnbin=%d, binsize=%d \n",Nbin,binsize);
    #endif
    
    checkPtr(h);
    if(!binsize){
        printf("\nBinsize is null!\n");
        exit(0);
    }
    if(Nbin<=0){
        printf("\nNbin is invalid!\n");
        exit(0);
    }
    
    fprintf(ISTO,"\n set boxwidth %d \n unset autoscale x\n set xrange [%lld:%lld] \n set style fill solid border -1 \n",binsize,xmin-2*binsize,xmax+2*binsize);
    fprintf(ISTO,"plot '-' u 1:2 smooth freq w boxes lc rgb 'red' title 'occorrenze', ");
    fprintf(ISTO,"%.20lf*exp(-x*x/%d) lw 4 lc rgb '#008080' title 'PDF' \n ",1./sqrt(2.*M_PI*Nsteps),2*Nsteps);
    
    for(i=0;i<Nstorie;i++){
        j=(int)(xfinale[i]-xmin)/binsize;
        h[j]++;
    }
    
    tmp=1./(long double)(binsize*Nstorie);    
    for(i=0;i<Nbin;i++){
        fprintf(ISTO,"%lld %.30Lf \n",i*binsize+xmin,h[i]*tmp); //normalizzo e plotto istogramma
    }
    
    chiudiPipe(ISTO);
    free(xfinale);
    #endif
    /* istogramma 1D */
    
    
    /* istogramma 2D */
    #if D==2
    INT **h = createMatrix(Nsteps*2,Nsteps*2); //matrice con la quantita di occorrenze per ogni punto (x,y)
    
    checkPtr(h);
    
    fprintf(ISTO,"\n unset autoscale xy\n set xrange [%lld:%lld] \n set yrange [%lld:%lld] \n",xmin[0],xmax[0],xmin[1],xmax[1]);
    fprintf(ISTO,"set zlabel 'P(x_{finale})' \n");
    fprintf(ISTO,"splot '-' u 1:2:3 pt 7 ps 0.5 lc rgb '#008080' notitle \n");
    
    for(j=0;j<Nstorie;j++){
        h[xfinale[j][0]+Nsteps][xfinale[j][1]+Nsteps]++;
        #ifdef DEBUG 
        printf ("\nj(storia)=%d, xfinale0=%lld, xfinale1=%lld, xmin0=%lld, xmax0=%lld, xmin1=%lld, xmax1=%lld",j,xfinale[j][0],xfinale[j][1],xmin[0],xmax[0],xmin[1],xmax[1]);
        #endif
    }
    
    tmp=1./Nstorie;
    
    for(j=0;j<Nsteps*2;j++){
        for(i=0;i<Nsteps*2;i++){
            fprintf(ISTO,"%d %d %.30Lf \n",i-Nsteps,j-Nsteps,(long double)(h[i][j]*tmp));
        }
    }
    
    chiudiPipe(ISTO);
    freeMatrix(Nsteps*2,h);
    freeMatrix(Nstorie,xfinale);
    #endif
    /* istogramma 2D */
    
    free(distanze);
    free(x);
    chiudiPipe(DISTANZE);
    
    return 0;
}

void transiente (int scarti, double (*metodo)(void)){
    int i;
    
    for(i=0; i<scarti; i++)
        metodo();
    
    return;
}

double ecuyer1 (void){
    static UINT a = 1181783497276652981ULL;
    I *= a;
    
    return (double)(I*ULL);
}

double rand1 (void) {
    static double a = 1./(RAND_MAX+1.);
    
    return rand()*a;
}

double random1 (void) {
    static double a = 1./(RAND_MAX+1.);
    
    return random()*a;
}

double minstd (void) {
    static int a = 16807;
    static unsigned long int m = 2147483647;
    I = (a*I)%m;
    
    return (double)I/(double)m;
}

UINT initSeed (int a){
    int i;
    unsigned char buf1[6];
    char buf2[20];
    FILE *input; 
    if(a==1) input = fopen("/dev/urandom", "r");
    else input = fopen("/dev/random", "r");
    FILE *f = fmemopen(buf2, 20*sizeof(char), "w");
    
    if(!input || !f) return time(0);
    fscanf(input, "%6s", buf1);
    for(i = 0; i < 6; ++i) fprintf(f,"%d", buf1[i]);
    
    fclose(f);
    fclose(input);
    
    return (UINT)(atoll(buf2));
}

void init_pipe(FILE *pipe, char *xlabel, char *ylabel, char *titolo, char *nome_grafico, char *plotCmd){
    
    fprintf(pipe,"set autoscale \n");
    
    #ifdef PNG
    fprintf(pipe,"set terminal png size %d,%d\n",(int)pSize,(int)(pSize*0.9133333)); // imposta la risoluzione dell'immagine in base alla dimensione della griglia di incrementi
    #else
    fprintf(pipe,"set terminal x11 \n");
    #endif  
    
    fprintf(pipe,"set xlabel %s \n",xlabel);
    fprintf(pipe,"set ylabel %s \n",ylabel);
    fprintf(pipe,"set title %s \n",titolo);
    
    #ifdef PNG
    fprintf(pipe,"set output %s \n",nome_grafico);
    #endif  
    
    if(!strcmp(plotCmd,"")) fprintf(pipe,"plot '-' u 1:2 w l \n");
    else fprintf(pipe,"%s \n", plotCmd);
    
    return;
}

void controllo_gnuplot(void){
    FILE *output = fopen("/usr/bin/gnuplot","r");
    
    if(output == NULL){
        printf("\nInstalla gnuplot con: sudo apt install gnuplot\n\n");
        exit(1);
    }
    
    return;
}

void chiudiPipe(FILE *pipe){
    
    fflush(pipe);
    fprintf(pipe,"quit\n");
    pclose(pipe);
    
    return;
}

INT **createMatrix(long long int rows, long long int columns){
    long long int i;
    
    INT **M = (INT **) calloc(rows, sizeof(INT *));
    checkPtr(M);
    
    for(i=0; i<rows; i++){
        M[i] = (INT *) calloc(columns, sizeof(INT));
        checkPtr(M[i]);
    }
    
    return M;
}

void freeMatrix(long long int rows, INT **M){
    
    while(--rows > -1){
        free(M[rows]);
    }
    
    free(M);
    
    return ;
}

void checkPtr(void *ptr){
    
    if (ptr==NULL){
        perror("\nERROR");
        fprintf(stderr,"\n");
        exit(0);
    }
    
}

