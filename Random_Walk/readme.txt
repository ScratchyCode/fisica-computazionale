per avviare il programma lanciare lo script "start.sh"
per cambiare le condizioni iniziali ed i parametri modificare il file "condizioni_iniziali.dat"

questo programma simula random walk in un numero arbitrario (D) di dimensioni
per inserire il valore di D (dimensioni) dare il comando -D D=... durante la compilazione

i plot vengono salvati in formato ".png" nella directory di esecuzione (se è definita la variabile da precompilatore "PNG")
gli istogrammi vengono plottati solo se D<3, invece le distanze euclidee dall'origine vengono plottate sempre

il proramma contiene 5 diversi generatori di numeri casuali normalizzati all'intervallo [0,1) e contiene una funzione per 
generare un seed "buono" (migliore di time(0) almeno) a partire da /dev/urandom o /dev/random quando possibile
questa funzione si è resa necessaria, in quanto in fase di analisi dei risultati è emerso che i generatori drand48 e minimal
standard sono molto sensibili al seed iniziale: se il seed aumenta poco alla volta sequenzialmente (come in effetti accade con time(0)
se il programma viene lanciato diverse volte a catena) il numero casuale generato a un determinato step (anche dopo un certo transiente)
decresce insieme al crescere del seed, per poi ripartire da 1 quando raggiunge lo 0

