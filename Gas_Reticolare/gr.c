// Scritto da Pietro Squilla e Francesco Valerio Servilio
#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <time.h>
#include <limits.h>
#include <string.h>
#include <unistd.h>

// #define DEBUG 1
#define PROG 1
#define TRANSIENTE 12345
#define RHO_CRITICA 0.25

typedef struct punto{
    int x;
    int y;
} punto;

void transiente (int scarti, double (*metodo)(void));
int **createMatrix(long long int rows, long long int columns);
void initMatrix(long long int rows, long long int columns, int **M, int value);
void readMatrix(long long int rows, long long int columns, int **M, char file[]);
void freeMatrix(long long int rows, int **M);
int **transposeMatrix(long long int rows, long long int columns, int **M);
void printMatrix(long long int rows, long long int columns, int **M);
double distanza2(punto *PosParticella, punto *PosParticella0, int Npart);
int mod(int a, int b);
void checkPtr (void *ptr);

int main(int argc, char **argv){
    long long int Npart, n;
    int L, Nsteps, Nstorie, i, j, k;
    int posx, posy, posxS, posyS, up, down, right, left, drawn;
    double rho;
    FILE *coeff = fopen("coeff.dat","w");
    FILE *input = fopen("condizioni_iniziali.dat","r");
    
    checkPtr(input);
    
    #ifdef DEBUG
//     FILE *output = fopen("log.txt","w");
    FILE *output = stderr;
    checkPtr(output);
    #endif
    
    fscanf(input,"%d %lf %d %d\n", &L, &rho, &Nsteps, &Nstorie);
    fclose(input);
    
    // densita' normalizzata
    if(rho >= 1 || rho <= 0){
        fprintf(stderr,"\nLa densita' deve essere compresa tra 0 e 1!\n");
        exit(0);
    }
    
    long double *R2tot = calloc(Nsteps+1, sizeof(long double));
    punto *PosParticella; // posizione di ogni particella (variabile)
    punto *PosParticella0; // posizione di ogni particella all'inizio (costante)
    punto *PosParticellaS; // posizione di ogni particella senza condizioni al contorno (variabile) 
    int **reticolo = createMatrix(L,L); // creo il reticolo
    
    checkPtr(reticolo);
    
    srand48(time(NULL));
    transiente(TRANSIENTE, drand48);
    
    #ifdef DEBUG
    fprintf(output,"Npart_inizio=%lld",(long long int)(L*L*rho));
    #endif
    
    #ifdef PROG
    printf("\n");
    #endif
    
    for(k=1;k<=Nstorie;k++){
        
        // popolo il reticolo
        n=0;
        Npart = (long long int)(L*L*rho); // numero atteso di particelle
        if(rho < RHO_CRITICA){ // se la densita' e' sotto al 25% di default esegue questo metodo per popolare
            PosParticella = (punto*)malloc((Npart+2)*sizeof(punto)); 
            PosParticella0 = (punto*)malloc((Npart+2)*sizeof(punto));
            PosParticellaS = (punto*)malloc((Npart+2)*sizeof(punto)); 
            
            while(n <= Npart){
                i = (int)(drand48()*L);
                j = (int)(drand48()*L);
                if(reticolo[i][j] == 0){
                    n++;
                    reticolo[i][j] = n; // il numero della particella sarà l'indice dell'array posizione
                    
                    PosParticella[n].x = i; 
                    PosParticella[n].y = j; 
                    PosParticella0[n] = PosParticella[n];
                    PosParticellaS[n] = PosParticella[n]; 
                    
                }
            }
        }
        else{
            PosParticella = (punto*)(malloc(sizeof(punto))); 
            PosParticella0 = (punto*)(malloc(sizeof(punto)));
            PosParticellaS = (punto*)(malloc(sizeof(punto))); 
            for(i=0; i<L; i++){
                for(j=0; j<L; j++){
                    if(drand48() < rho){
                        n++;
                        
                        reticolo[i][j] = n; // il numero della particella sarà l'indice dell'array posizione
                        
                        PosParticella=(punto*)realloc(PosParticella,(n+1)*sizeof(punto)); 
                        PosParticella0=(punto*)realloc(PosParticella0,(n+1)*sizeof(punto));
                        PosParticellaS=(punto*)realloc(PosParticellaS,(n+1)*sizeof(punto)); 
                        
                        PosParticella[n].x = i;
                        PosParticella[n].y = j;
                        PosParticella0[n] = PosParticella[n];
                        PosParticellaS[n] = PosParticella[n];      
                        
                    }
                }
            }
        }
        
        Npart=n;
        
        checkPtr(R2tot);
        checkPtr(PosParticella);
        checkPtr(PosParticella0);
        checkPtr(PosParticellaS);
        
        #ifdef DEBUG
        fprintf(output,"\nPopolamento finito, Npart=%lld\n",n);
        #endif
        
        // evoluzione reticolo
        for(i=1; i<=Nsteps; i++){
            
            // spostamento di ogni particella
            for(n=1; n<=Npart; n++){    
                
                //metodo alternativo:
                //          int ii;
                //          for(ii=1; ii<=Npart; ii++){
                //              n=(int)(drand48()*(Npart)+1.);
                
                posx=PosParticella[n].x;
                posy=PosParticella[n].y;
                posxS=PosParticellaS[n].x;
                posyS=PosParticellaS[n].y;
                
                #ifdef DEBUG
                if(n<1 || n>Npart){
                    fprintf(output,"\nERROR: how many particles? n = %lld\n", n);
                    exit(0);
                }
                if(reticolo[posx][posy]==0){
                    fprintf(output,"\nERROR: where is my particle?\n");
                    exit(0);
                }
                #endif
                
                up=mod((posy+1), L);
                down=mod((posy-1), L);
                right=mod((posx+1), L);
                left=mod((posx-1), L);
                
                drawn=(int)(4.*drand48());
                
                if(drawn==0 && reticolo[posx][up]==0){
                    reticolo[posx][posy]=0;
                    reticolo[posx][up]=n;
                    PosParticella[n].x=posx;
                    PosParticella[n].y=up;
                    PosParticellaS[n].x=posxS;
                    PosParticellaS[n].y=posyS+1;
                }else if(drawn==1 && reticolo[posx][down]==0){
                    reticolo[posx][posy]=0;
                    reticolo[posx][down]=n;
                    PosParticella[n].x=posx;
                    PosParticella[n].y=down;
                    PosParticellaS[n].x=posxS;
                    PosParticellaS[n].y=posyS-1;
                }else if(drawn==2 && reticolo[right][posy]==0){
                    reticolo[posx][posy]=0;
                    reticolo[right][posy]=n;
                    PosParticella[n].x=right;
                    PosParticella[n].y=posy;
                    PosParticellaS[n].x=posxS+1;
                    PosParticellaS[n].y=posyS;
                }else if(drawn==3 && reticolo[left][posy]==0){
                    reticolo[posx][posy]=0;
                    reticolo[left][posy]=n;
                    PosParticella[n].x=left;
                    PosParticella[n].y=posy;
                    PosParticellaS[n].x=posxS-1;
                    PosParticellaS[n].y=posyS;
                }else if(drawn>3){
                    printf("\nERROR: which is the neighbour?\n");
                }
                
            }
            
            R2tot[i]+=(long double)distanza2(PosParticellaS, PosParticella0, Npart);  //per ogni step, sommando sulle storie
            
            #ifdef DEBUG
            fprintf(output,"\n storia=%d r2tot[step=%d]=%.15Lf, dist=%lf",k,i, R2tot[i], distanza2(PosParticellaS, PosParticella0, Npart));
            if(distanza2(PosParticellaS, PosParticella0, Npart)<0){
                fprintf(output,"\nERROR: negative R^2\n");
                exit(0);
            }
            #endif
            
        }
        
        //svuoto array
        initMatrix(L,L,reticolo,0); 
        checkPtr(reticolo);
        free(PosParticella);
        free(PosParticellaS);
        free(PosParticella0);
        
        #ifdef PROG
        printf("\rCalcolo in corso:\t\t%2d%%",(int)(k*100./Nstorie));
        #endif
        
        #ifdef DEBUG
        fprintf(output,"\nfine giro\n");
        #endif
        
    }   //fine ciclo storie
    
    
    for(i=1; i<=Nsteps; i++){
        fprintf(coeff, "%d %.15lf\n", i, (double)(R2tot[i]/(2.*2.*i*Nstorie))); // stampo coefficiente di diffusione
    }
    
    free(R2tot);
    freeMatrix(L, reticolo);
    fclose(coeff);
    
    #ifdef DEBUG
    fclose(output);
    #endif
    
    printf("\rOperazione completata:\t\t100%%");
    
    return 0;
}






void transiente (int scarti, double (*metodo)(void)){
    int i;
    
    for(i=0; i<scarti; i++)
        metodo();
    
    return;
}

int **createMatrix(long long int rows, long long int columns){
    long long int i;
    
    int **M = (int **) calloc(rows, sizeof(int *));
    checkPtr(M);
    
    for(i=0; i<rows; i++){
        M[i] = (int *) calloc(columns, sizeof(int));
        checkPtr(M[i]);
    }
    
    return M;
}

void initMatrix(long long int rows, long long int columns, int **M, int value){
    long long int i, j;
    
    for(i=0; i<rows; i++) for(j=0; j<columns; j++) M[i][j] = value;
    
    return;
}

void readMatrix(long long int rows, long long int columns, int **M, char file[]){
    long long int i, j;
    
    FILE *input = fopen(file,"r");
    checkPtr(input);
    
    for(i=0; i<rows; i++){
        for(j=0; j<columns; j++){
            fscanf(input,"%d ",&M[i][j]);
        }
    }
    
    fclose(input);
    
    return ;
}

void freeMatrix(long long int rows, int **M){
    
    while(--rows > -1){
        free(M[rows]);
    }
    
    free(M);
    
    return ;
}

int **transposeMatrix(long long int rows, long long int columns, int **M){
    long long int i, j;
    int **transposedMatrix = createMatrix(rows,columns);
    
    for(i=0; i<rows; i++){
        for(j=0; j<columns; j++){
            transposedMatrix[j][i] = M[i][j];
        }
    }
    
    return transposedMatrix;
}

void printMatrix(long long int rows, long long int columns, int **M){
    long long int i, j;
    
    printf("\n");
    
    for(i=0; i<rows; i++){
        for(j=0; j<columns; j++){
            printf("%d ",M[i][j]);
        }
        printf("\n");
    }
    
    return ;
}

double distanza2 (punto *PosParticellaS, punto *PosParticella0, int Npart){
    unsigned long long int R2=0;
    int j;
    
    for(j=1; j<=Npart; j++){
        R2+=((PosParticellaS[j].x)-(PosParticella0[j].x))*((PosParticellaS[j].x)-(PosParticella0[j].x))+
        ((PosParticellaS[j].y)-(PosParticella0[j].y))*((PosParticellaS[j].y)-(PosParticella0[j].y));
    }
    
    return (double)R2/Npart; // distanza media delle particelle dal proprio punto di partenza
    
}

int mod(int a, int b){
    b=abs(b);
    int ret = a % b;
    
    if(ret < 0)
        ret+=b;
    
    return ret;
}


void checkPtr(void *ptr){
    #ifdef DEBUG
    if (ptr==NULL){
        perror("\nERROR");
        fprintf(stderr,"\n");
        exit(0);
    }
    #endif
}

