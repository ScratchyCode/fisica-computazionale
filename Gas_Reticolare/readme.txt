questo programma simula l'evoluzione di un reticolo bidimensionale popolato da particelle di gas
i parametri vengono inseriti nel file "condizioni_iniziali.dat"
si possono definire da precompilatore (comando -D durante la compilazione) le costanti PROG, per avere
su stdout la percentuale di avanzamento e DEBUG per avere dei controlli più pendanti sull'esecuzione
il programma salva un file di dati coeff.dat contenente il coefficiente di diffusione in funzione dello step

il programma viene compilato e lanciato con lo script start.sh
gli script DvsL.sh e DvsRho.sh lanciano diverse volte il programma, salvano i dati necessari (coefficienti
di diffusione e parametri) e poi plottano su un file .png il risultato della simulazione
i due script producono un file "errlog.txt" contenente eventuali errori di esecuzione (esso viene eliminato
automaticamente nel caso in cui sia vuoto)
