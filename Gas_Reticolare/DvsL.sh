#!/bin/bash
# Scritto da Pietro Squilla e Francesco Valerio Servilio
# questo script salva in una cartella il coefficiente 
# di diffusione in funzione dello step, per vari valori di L

date
rm -rf DvsL
rm -f .condizioni_iniziali_bak.dat gr.exe errlog.txt DvsL.png
mv condizioni_iniziali.dat .condizioni_iniziali_bak.dat >/dev/null 2>/dev/null
mkdir DvsL
gcc gr.c -o gr.exe -lm -pedantic -Wall -ffast-math -O2

Nstorie=2000
Nsteps=2000
rho=0.6

esegui() {
    L=$1
    echo $L $rho $Nsteps $Nstorie > condizioni_iniziali.dat
    ./gr.exe >/dev/null 2>>./errlog.txt
    mv coeff.dat $(printf './DvsL/coeffL%s.dat' $L)
}

for i in $(seq 10 10 40) 60 80
    do
        esegui $i
    done

find -size 0 -name 'errlog.txt' -delete
rm gr.exe
mv .condizioni_iniziali_bak.dat condizioni_iniziali.dat >/dev/null 2>/dev/null


# per plottare su gnuplot:
cd DvsL
lista=$(ls | grep -h coeffL | cut -c7- | cut -d "." -f 1);
lista=$(echo $lista)

gnuplot << EOF
set terminal png size 2000,1800;
set output "DvsL.png";
set xlabel "step";
set ylabel "D";
set title "Nstorie=$Nstorie, rho=$rho, Nsteps=$Nsteps";
filename(n) = sprintf("coeffL%s.dat", n);
plot for [i in "$lista"] filename(i) u 1:2 lw 0.5 w l title sprintf("L=%s",i);
EOF

mv DvsL.png ../DvsL.png
cd .. 

exit 0
