#!/bin/bash
# Scritto da Pietro Squilla e Francesco Valerio Servilio
# questo script salva in una cartella il coefficiente 
# di diffusione allo step finale, per vari valori di rho

date
rm -f .condizioni_iniziali_bak.dat gr.exe errlog.txt DvsRho.dat DvsRho.png
mv condizioni_iniziali.dat .condizioni_iniziali_bak.dat >/dev/null 2>/dev/null
gcc gr.c -o gr.exe -lm -pedantic -Wall -ffast-math -O2

Nstorie=150
Nsteps=15000
L=20

esegui() {
    rho=$1
    echo $L $rho $Nsteps $Nstorie > condizioni_iniziali.dat
    ./gr.exe >/dev/null 2>>./errlog.txt
    echo $rho $(tail -1 coeff.dat) >> DvsRho.dat
}

LANG=en_US 
for i in $(seq 0.1 0.1 0.9)
    do
        esegui $i
    done

find -size 0 -name 'errlog.txt' -delete
rm gr.exe coeff.dat
mv .condizioni_iniziali_bak.dat condizioni_iniziali.dat >/dev/null 2>/dev/null

# per plottare su gnuplot:
gnuplot << EOF
set terminal png size 2000,1800;
set output "DvsRho.png";
set title "t_{max}=$Nsteps, L=$L";
set xlabel "rho";
set ylabel "D(t_{max})";
plot "DvsRho.dat" u 1:3 lw 0.8 ps 1.5 pt 7 w linespoints notitle;
EOF

exit 0 
