#!/bin/bash
# Coded by Pietro Squilla & Francesco Servilio

# entro dentro la cartella con gli eseguibili
DIR=$(dirname "$0")
cd $DIR

killall gnuplot_x11 -q 
killall gnuplot_qt -q 

# compilo ed eseguo il programma
gcc rwre.c -o rwre.exe -lm -pedantic -Wall -ffast-math -O2

date

time ./rwre.exe -D DEBUG=0 &

wait $!
rm rwre.exe

# elimina le immagini vuote
find -size 0 -name "*.png" -delete

exit 0
