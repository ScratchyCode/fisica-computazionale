simula un random walk in ambiente aleatorio: la probabilita di andare a destra o a sinistra
dipende dalla posizione (ma non dal tempo): viene innanzitutto creato un reticolo (in questo caso
unidimensionale) contenente le probabilità per ogni punto dello spazio, e poi si simula il moto
di una particella soggetta all'agitazione termica (random walk) e a un qualche altro rumore "congelato"
(probabilità di spostamento non omogenea)

per avviare il programma lanciare lo script "start.sh"
per cambiare le condizioni iniziali ed i parametri modificare il file "condizioni_iniziali.dat"
