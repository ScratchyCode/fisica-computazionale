// Scritto da Pietro Squilla e Francesco Valerio Servilio
#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <time.h>
#include <limits.h>
#include <string.h>
#include <unistd.h>


// #define DEBUG 1
#define DEBUG1 1
#define PRINTDATI
#define PNG 1 
#define pSize 1000.0 

typedef long long int INT; // %lld
typedef unsigned long long int UINT; // %llu

void transiente(int scarti, double (*metodo)(void));
double ecuyer1(void);
void controllo_gnuplot(void);
void init_pipe(FILE *pipe, char *xlabel, char *ylabel, char *titolo, char *nome_grafico, char *plotCmd);
void chiudiPipe(FILE *pipe);
INT **createMatrix(long long int rows, long long int columns);
void freeMatrix(long long int rows, INT **M);
void checkPtr(void *ptr);
double rand1(void);
double random1(void);
double minstd(void);
UINT initSeed(int a);

double ULL = 1./(ULLONG_MAX+1.);
UINT I;

int main(void) {
    int Nstorie, Nsteps, algo;
    UINT seed = initSeed(2);
    int i, j=0, binsize=0;
    double r;
    
    // scanf da file
    FILE *input = fopen("condizioni_iniziali.dat","r");
    checkPtr(input);
    fscanf(input,"%d %d %d %d\n", &Nstorie, &Nsteps, &algo, &binsize);
    fclose(input);
    
    controllo_gnuplot();
    
    INT x;
    UINT *distanze = (UINT*)calloc(Nsteps,sizeof(UINT));
    double (*metodo)(void);
    double *pp = (double*)malloc((Nsteps+1)*sizeof(double));
    double *pm = (double*)malloc((Nsteps+1)*sizeof(double));
    checkPtr(pp);
    checkPtr(pm);
    FILE *DATI = fopen("rwre.dat","w");
    FILE *DISTANZE = popen("/usr/bin/gnuplot -persist 2> /dev/null","w");
    
    init_pipe(DISTANZE,"\"t\"","\"<x^2>\"","\"media (sulle storie) delle distanze euclidee al quadrato in funzione di t (step): scala semi-log\"","\"distanze.png\"","set logscale x \n plot 0.11225*((log(x))**4) lw 1 title 'y=A log(x)^4', '-' u 1:2 ps 0.1 pointtype 7 lc rgb 'blue' title 'distanze al quadrato' ");
//     init_pipe(DISTANZE,"\"t\"","\"<x^2>\"","\"media (sulle storie) delle distanze euclidee al quadrato in funzione di t (step)\"","\"distanze.png\""," \n plot 0.1124*(log(x))**4 lw 1 title 'y=A log(x)^4', '-' u 1:2 ps 0.1 pointtype 7 lc rgb 'blue' title 'distanze al quadrato' ");
    
    // istogrammi
    INT xmax=-Nsteps, xmin=Nsteps;
    INT *xfinale=(INT*)calloc(Nstorie,sizeof(INT));
    checkPtr(xfinale);
    
    FILE *ISTO = popen("/usr/bin/gnuplot -persist 2> /dev/null","w");
    init_pipe(ISTO,"\"x\"","\"P(x_{finale})\"","\"Istogramma posizioni finali\"","\"isto.png\"","\n");
    
    // scelta generatore
    switch(algo){
        default:
            metodo = drand48;
            srand48(seed);
            break;
        case 1:
            metodo = drand48;
            srand48(seed);
            break;
        case 2:
            metodo = ecuyer1;
            I = seed;
            break;
        case 3:
            metodo = rand1;
            srand(seed);
            break;
        case 4:
            metodo = random1;
            srandom(seed);
            break;
        case 5:
            metodo = minstd;
            I = seed;
            break;
    }
    
    transiente(12345, metodo);
    
 
    #ifdef DEBUG
    printf("\npp[0]=%lf\n",pp[0]);
    #endif 
    
    for(j=0; j<Nstorie; j++){
        
        // genero l'ambiente casuale
        for(i=0; i<=Nsteps; i++) {
            pp[i]=metodo();         // probabilità di andare a sn per x positive
            pm[i]=metodo();         // probabilità di andare a sn per x negative
        }
        
        x=0; // azzero la posizione
        
        for(i=0; i<Nsteps; i++){
            
            r = metodo();
            
            if(x>=0) x += (int)( ((r > pp[x])-0.5)*2. );
            else x += (int)( ((r > pm[llabs(x)])-0.5)*2. );
            
            #ifdef DEBUG
            if(!j&&x>=0) printf("%d\n", (int)( ((r > pp[x])-0.5)*2. ));
            if(!j&&x<0) printf("\t  %d \n", (int)( ((r > pm[llabs(x)])-0.5)*2. ));
            #endif
            
            #ifdef PRINTDATI
            fprintf(DATI,"S %d T %d x %lld \n",j+1, i+1, x);
            distanze[i] += x*x; // salvo distanza euclidea al quadrato dall'origine
            #endif    
            
        }
        
        if(x > xmax) xmax = x;
        else if(x < xmin) xmin = x; 
        xfinale[j] = x; //x finale per ogni storia
        
    }
    
    fclose(DATI); // chiudo il file di dati
    
    #ifdef PRINTDATI
    for(i=0; i<Nsteps; i++) fprintf(DISTANZE,"%d %.15lf\n",i,(double)distanze[i]/Nstorie);     //plotta le distanze euclidee
    #endif
    
    
    /* istogramma */
    if(!binsize) { // dipendenza trovata empiricamente
        if(Nstorie>=Nsteps) binsize = (int)(3.*pow(Nsteps,2./3.)*pow(Nstorie,2./3.)/20000.)+1;
        if(Nstorie<Nsteps) binsize = (int)(3.*Nstorie/120.)+1;
    }
    int Nbin = (int)((xmax-xmin+1.)/binsize)+1;
    double *h = (double*)calloc(Nbin+2,sizeof(double));
    
    #ifdef DEBUG
    printf ("\nxfinale1=%lld, xmin=%lld, xmax=%lld",xfinale[1],xmin,xmax);
    printf("\nnbin=%d, binsize=%d \n",Nbin,binsize);
    #endif
  
    if(!binsize){
        printf("\nBinsize is null!\n");
        exit(0);
    }
    if(Nbin<=0){
        printf("\nNbin is invalid!\n");
        exit(0);
    }
    checkPtr(h);
    
    fprintf(ISTO,"\n set boxwidth %d \n unset autoscale x\n set xrange [%lld:%lld] \n set style fill solid border -1 \n",binsize,xmin-2*binsize,xmax+2*binsize);
    fprintf(ISTO,"plot '-' u 1:2 smooth freq w boxes lc rgb 'red' title 'occorrenze'");
    
    for(i=0;i<Nstorie;i++){
        j=(int)(xfinale[i]-xmin)/binsize;
        h[j]++;
    }
    
    long double tmp=1./(long double)(binsize*Nstorie);   
    for(i=0;i<Nbin;i++){
        fprintf(ISTO,"%lld %.30Lf \n",i*binsize+xmin,h[i]*tmp); //normalizzo e plotto istogramma
    }
    /* istogramma */
    
    
    chiudiPipe(ISTO);
    free(xfinale);
    free(distanze);
    free(pm);
    free(pp);
    chiudiPipe(DISTANZE);
    
    return 0;
}

void transiente (int scarti, double (*metodo)(void)){
    int i;
    
    for(i=0; i<scarti; i++)
        metodo();
    
    return;
}

double ecuyer1 (void){
    static UINT a = 1181783497276652981ULL;
    I *= a;
    
    return (double)(I*ULL);
}

double rand1 (void) {
    static double a = 1./(RAND_MAX+1.);
    
    return rand()*a;
}

double random1 (void) {
    static double a = 1./(RAND_MAX+1.);
    
    return random()*a;
}

double minstd (void) {
    static int a = 16807;
    static unsigned long int m = 2147483647;
    I = (a*I)%m;
    
    return (double)I/(double)m;
}

UINT initSeed (int a){
    int i;
    unsigned char buf1[6];
    char buf2[20];
    FILE *input; 
    if(a==1) input = fopen("/dev/urandom", "r");
    else input = fopen("/dev/random", "r");
    FILE *f = fmemopen(buf2, 20*sizeof(char), "w");
    
    if(!input || !f) return time(0);
    fscanf(input, "%6s", buf1);
    for(i = 0; i < 6; ++i) fprintf(f,"%d", buf1[i]);
    
    fclose(f);
    fclose(input);
    
    return (UINT)(atoll(buf2));
}

void init_pipe(FILE *pipe, char *xlabel, char *ylabel, char *titolo, char *nome_grafico, char *plotCmd){
    
    fprintf(pipe,"set autoscale \n");
    
    #ifdef PNG
    fprintf(pipe,"set terminal png size %d,%d\n",(int)pSize,(int)(pSize*0.9133333)); // imposta la risoluzione dell'immagine in base alla dimensione della griglia di incrementi
    #else
    fprintf(pipe,"set terminal x11 \n");
    #endif  
    
    fprintf(pipe,"set xlabel %s \n",xlabel);
    fprintf(pipe,"set ylabel %s \n",ylabel);
    fprintf(pipe,"set title %s \n",titolo);
    
    #ifdef PNG
    fprintf(pipe,"set output %s \n",nome_grafico);
    #endif  
    
    if(!strcmp(plotCmd,"")) fprintf(pipe,"plot '-' u 1:2 w l \n");
    else fprintf(pipe,"%s \n", plotCmd);
    
    return;
}

void controllo_gnuplot(void){
    FILE *output = fopen("/usr/bin/gnuplot","r");
    
    if(output == NULL){
        printf("\nInstalla gnuplot con: sudo apt install gnuplot\n\n");
        exit(1);
    }
    
    return;
}

void chiudiPipe(FILE *pipe){
    
    fflush(pipe);
    fprintf(pipe,"quit\n");
    pclose(pipe);
    
    return;
}

INT **createMatrix(long long int rows, long long int columns){
    long long int i;
    
    INT **M = (INT **) calloc(rows, sizeof(INT *));
    checkPtr(M);
    
    for(i=0; i<rows; i++){
        M[i] = (INT *) calloc(columns, sizeof(INT));
        checkPtr(M[i]);
    }
    
    return M;
}

void freeMatrix(long long int rows, INT **M){
    
    while(--rows > -1){
        free(M[rows]);
    }
    
    free(M);
    
    return ;
}

void checkPtr(void *ptr){
    
    if (ptr==NULL){
        perror("\nERROR");
        fprintf(stderr,"\n");
        exit(0);
    }
    
}

